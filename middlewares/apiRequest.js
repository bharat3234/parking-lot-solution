'use strict';

const { respondError } = require('../helpers/response');
const generalLogger = require('../config/winston');

module.exports = {

  requireApiKey: (req, res, next) => {
    const apiKey = req.header('x-api-key');
    const headersObj = { 'x-api-key': apiKey };
    if (req.header('authorization')) {
      headersObj['authorization'] = req.header('authorization');
    }
    console.log('\n');
    generalLogger.log('info', JSON.stringify(headersObj));
    generalLogger.log('info', JSON.stringify(req.body));
    if (!apiKey) {
      return next(respondError(403, 'x-api-key is required in headers'));
    }
    if (apiKey !== process.env.API_KEY) {
      return next(respondError(403, 'Invalid api-key'));
    }
    return next();
  },

};
