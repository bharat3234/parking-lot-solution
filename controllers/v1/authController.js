'use strict';

// node modules
const jwt = require('jsonwebtoken');

// models
const User = require('../../models/User');

// helpers
const { respondSuccess, respondFailure, respondError } = require('../../helpers/response');
const { getMessageFromValidationError } = require('../../helpers/utils');
const { validateSignUp, validateSignIn } = require('../../helpers/inputValidation');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.sign({
    sub: user.id,
    iat: timestamp,
    expiresIn: '4h',
  }, process.env.JWT_SECRET);
}

module.exports = {

  signUp: async(req, res, next) => {
    const { body } = req;
    const { email } = body;

    const { error } = validateSignUp(body);
    if (error) {
      return next(respondError(400, getMessageFromValidationError(error)));
    }

    const userExist = await User.findOne({ email: email.toLowerCase() }).lean();
    if (userExist) {
      return respondFailure(202, res, 'The email address you have entered is already registered');
    }
    const newUser = new User({ ...body, email: body.email.toLowerCase() });
    await newUser.save();
    return respondSuccess(201, res, 'User registered successfully');
  },

  signIn: async(req, res, next) => {
    const { body } = req;
    const { email, password } = body;

    const { error } = validateSignIn(req.body);
    if (error) {
      return next(respondError(400, getMessageFromValidationError(error)));
    }

    const userExists = await User.findOne({ email: email.toLowerCase() }).select('userName email password');
    if (!userExists) {
      return respondFailure(404, res, 'User not found, Please register');
    }
    userExists.comparePassword(password, (error, isMatch) => {
      if (error) {
        return respondFailure(500, res, 'Server is busy, Please try after sometime');
      }
      if (!isMatch) {
        return respondFailure(401, res, 'You entered wrong credentials');
      }
      const tokenID = tokenForUser(userExists);
      return respondSuccess(200, res, 'Logged in successfully', { token: tokenID, data: userExists });
    });
  },

};
