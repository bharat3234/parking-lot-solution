'use strict';

// models
const User = require('../../models/User');

// helpers
const { respondSuccess, respondFailure, respondError } = require('../../helpers/response');

module.exports = {

  users: async (req, res, next) => {
    const allUsers = await User.find({})
      .select('userName email status createdAt')
      .sort({ createdAt: -1 })
      .lean();
    return respondSuccess(200, res, 'All users fetched successfully', allUsers);
  },

  usersWithPagiation: async (req, res, next) => {
    const { skip, limit } = req.params;
    const usersCount = await User.countDocuments({});
    const limitedUsers = await User.find({})
      .select('userName email status createdAt')
      .sort({ createdAt: -1 })
      .skip(Number(skip))
      .limit(Number(limit))
      .lean();
    return respondSuccess(200, res, 'Users fetched successfully', { total: usersCount, users: limitedUsers });
  },

};
