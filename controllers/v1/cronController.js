'use strict';

//node_modules
const moment = require('moment');

// models
const ParkingSpace = require('../../models/ParkingSpace');

// helpers
const constValues = require('../../helpers/constants');

module.exports = {

  releaseSpace: async () => {
    console.log('\nCRON STARTED AT ', moment().format('YYYY-MM-DD HH:mm:ss'));
    const allSpaces = await ParkingSpace.countDocuments({});
    const bookedSpaces = await ParkingSpace.countDocuments({ isBooked: constValues.status.ACTIVE });
    const halfCapacity = (constValues.maxCapacityPercentage / 100) * allSpaces;
    const unAllocatedSpaces = await ParkingSpace.find({ isAllocated: constValues.status.DEACTIVE }).lean();
    console.log('Total Booked but UnAllocated Parking Lots :', unAllocatedSpaces.length);
    for (const space of unAllocatedSpaces) {
      const currentTimeStamp = moment(new Date());
      const arrivalTimeStamp = moment(space.arrivalTime);
      const minutes = moment.duration(currentTimeStamp.diff(arrivalTimeStamp)).asMinutes();
      console.log('Minutes exceeded from arrival time for Space Number :', space.spaceNumber, 'is ', minutes);
      if (bookedSpaces >= halfCapacity) {
        if (minutes >= 0) {
          await ParkingSpace.updateOne({ _id: space._id }, { $set: { isBooked: constValues.status.DEACTIVE } });
          await ParkingSpace.updateOne({ _id: space._id }, {
            $unset: {
              bookedBy: 1, bookedOn: 1, arrivalTime: 1,
              isAllocated: 1, allocatedOn: 1
            }
          });
          console.log('Space Number :', space.spaceNumber, 'will be released automatically');
        }
      } else {
        if (minutes >= constValues.waiTimeInMinutes) {
          await ParkingSpace.updateOne({ _id: space._id }, { $set: { isBooked: constValues.status.DEACTIVE } });
          await ParkingSpace.updateOne({ _id: space._id }, {
            $unset: {
              bookedBy: 1, bookedOn: 1, arrivalTime: 1,
              isAllocated: 1, allocatedOn: 1
            }
          });
          console.log('Space Number :', space.spaceNumber, 'will be released automatically');
        }
      }
    }
    console.log('CRON ENDED AT ', moment().format('YYYY-MM-DD HH:mm:ss'));
  },

};
