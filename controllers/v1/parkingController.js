'use strict';

//node_modules
const moment = require('moment');

// models
const User = require('../../models/User');
const ParkingSpace = require('../../models/ParkingSpace');

// helpers
const { respondSuccess, respondFailure, respondError } = require('../../helpers/response');
const { getMessageFromValidationError } = require('../../helpers/utils');
const { validateBookSpace, validateAllocateSpace, validateReleaseSpace } = require('../../helpers/inputValidation');
const constValues = require('../../helpers/constants');

module.exports = {

  createSpace: async (req, res, next) => {
    ParkingSpace.collection.drop();
    const reservedSpace = (constValues.reservedPercentage / 100) * constValues.totalSpaces;
    for (let i = 0; i < constValues.totalSpaces; i++) {
      if (i < reservedSpace) {
        await new ParkingSpace({ spaceNumber: i + 1, isReserved: constValues.status.ACTIVE }).save();
      } else {
        await new ParkingSpace({ spaceNumber: i + 1 }).save();
      }
    }
    return respondSuccess(201, res, 'Spaces created successfully');
  },

  bookSpace: async (req, res, next) => {
    const { body, user } = req;
    const { id } = user;
    const { arrivalTime, reservedBooking } = body;

    const { error } = validateBookSpace(body);
    if (error) {
      return next(respondError(400, getMessageFromValidationError(error)));
    }

    const currentTimeStamp = moment(new Date());
    const arrivalTimeStamp = moment(arrivalTime);
    const minutes = moment.duration(arrivalTimeStamp.diff(currentTimeStamp)).asMinutes();
    if (minutes < 0) {
      return respondFailure(200, res, 'Invalid time selection');
    }
    if (15 > minutes) {
      return respondFailure(200, res, 'You can book a parking space 15 mins prior to arrival');
    }
    if (!reservedBooking) {
      const generalSpace = await ParkingSpace.findOne({ isReserved: constValues.status.DEACTIVE, isBooked: constValues.status.DEACTIVE }).lean();
      if (!generalSpace) {
        return respondFailure(200, res, 'No space available to book');
      }
      await ParkingSpace.updateOne({ _id: generalSpace._id }, {
        $set: {
          isBooked: constValues.status.ACTIVE, bookedOn: new Date(),
          bookedBy: id, arrivalTime: arrivalTime, isAllocated: constValues.status.DEACTIVE
        }
      });
      return respondSuccess(200, res, 'Space booked successfully', { parkingNumber: generalSpace.spaceNumber });
    }
    const reservedSpace = await ParkingSpace.findOne({ isReserved: constValues.status.ACTIVE, isBooked: constValues.status.DEACTIVE }).lean();
    if (reservedSpace) {
      await ParkingSpace.updateOne({ _id: reservedSpace._id }, {
        $set: {
          isBooked: constValues.status.ACTIVE, bookedOn: new Date(),
          bookedBy: id, arrivalTime: arrivalTime, isAllocated: constValues.status.DEACTIVE
        }
      });
      return respondSuccess(200, res, 'Space booked successfully', { parkingNumber: reservedSpace.spaceNumber });
    }
    const generalSpaceForReserved = await ParkingSpace.findOne({ isReserved: constValues.status.DEACTIVE, isBooked: constValues.status.DEACTIVE }).lean();
    if (generalSpaceForReserved) {
      await ParkingSpace.updateOne({ _id: generalSpaceForReserved._id }, {
        $set: {
          isBooked: constValues.status.ACTIVE, bookedOn: new Date(),
          bookedBy: id, arrivalTime: arrivalTime, isAllocated: constValues.status.DEACTIVE
        }
      });
      return respondSuccess(200, res, 'Space booked successfully', { parkingNumber: generalSpaceForReserved.spaceNumber });
    }
    return respondFailure(200, res, 'No space available to book');
  },

  allocateSpace: async (req, res, next) => {
    const { body, user } = req;
    const { id } = user;
    const { spaceNumber } = body;

    const { error } = validateAllocateSpace(body);
    if (error) {
      return next(respondError(400, getMessageFromValidationError(error)));
    }

    const spaceExists = await ParkingSpace.findOne({ spaceNumber: spaceNumber }).lean();
    if (!spaceExists) {
      return respondFailure(200, res, 'Wrong space number');
    }
    if (!spaceExists.isBooked) {
      return respondFailure(200, res, 'Space not yet booked');
    }
    if (String(spaceExists.bookedBy) !== String(id)) {
      return respondFailure(200, res, 'Space booked by other user');
    }
    if (spaceExists.isAllocated) {
      return respondFailure(200, res, 'Space already allocated');
    }
    await ParkingSpace.updateOne({ spaceNumber: spaceNumber }, { $set: { isAllocated: constValues.status.ACTIVE, allocatedOn: new Date() } });
    return respondSuccess(200, res, 'Space allocated successfully');
  },

  releaseSpace: async (req, res, next) => {
    const { body, user } = req;
    const { id } = user;
    const { spaceNumber } = body;

    const { error } = validateReleaseSpace(body);
    if (error) {
      return next(respondError(400, getMessageFromValidationError(error)));
    }

    const spaceExists = await ParkingSpace.findOne({ spaceNumber: spaceNumber }).lean();
    if (!spaceExists) {
      return respondFailure(200, res, 'Wrong space number');
    }
    if (!spaceExists.isBooked) {
      return respondFailure(200, res, 'Space not yet booked');
    }
    if (String(spaceExists.bookedBy) !== String(id)) {
      return respondFailure(200, res, 'Space booked by other user');
    }
    if (!spaceExists.isAllocated) {
      return respondFailure(200, res, 'Space not yet allocated');
    }
    await ParkingSpace.updateOne({ _id: spaceExists._id }, { $set: { isBooked: constValues.status.DEACTIVE } });
    await ParkingSpace.updateOne({ _id: spaceExists._id }, {
      $unset: {
        bookedBy: 1, bookedOn: 1, arrivalTime: 1,
        isAllocated: 1, allocatedOn: 1
      }
    });
    return respondSuccess(200, res, 'Space is now available for Re-booking');
  },

  getBookedSpaces: async (req, res, next) => {
    const bookedSpaces = await ParkingSpace.aggregate([
      {
        $match: {
          isBooked: constValues.status.ACTIVE
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: 'bookedBy',
          foreignField: '_id',
          as: 'user'
        }
      },
      {
        $unwind: '$user'
      },
      {
        $project: {
          _id: 0,
          spaceNumber: 1,
          isBooked: 1,
          bookedBy: '$user.userName',
          bookedOn: 1,
          isReached: '$isAllocated',
          reachedAt: '$allocatedOn',
        }
      }
    ])
    return respondSuccess(200, res, 'Booked spaces fetched successfully', bookedSpaces);
  },

  getAvailableSpaces: async (req, res, next) => {
    const bookedSpaces = await ParkingSpace.find({ isBooked: constValues.status.DEACTIVE })
      .select('-_id spaceNumber isBooked')
      .lean();
    return respondSuccess(200, res, 'Available spaces fetched successfully', bookedSpaces);
  },

  getAllSpaces: async (req, res, next) => {
    const { skip, limit } = req.params;
    const allSpaces = await ParkingSpace.aggregate([
      {
        $lookup: {
          from: 'users',
          localField: 'bookedBy',
          foreignField: '_id',
          as: 'user'
        }
      },
      {
        $unwind: {
          path: '$user',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project: {
          _id: 0,
          spaceNumber: 1,
          isBooked: 1,
          bookedBy: '$user.userName',
          bookedOn: 1,
          isReached: '$isAllocated',
          reachedAt: '$allocatedOn',
        }
      },
      {
        $skip: Number(skip)
      },
      {
        $limit: Number(limit)
      }
    ])
    return respondSuccess(200, res, 'All spaces fetched successfully', allSpaces);
  },

};
