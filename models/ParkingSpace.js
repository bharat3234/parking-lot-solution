'use strict';

const mongoose = require('mongoose');
const { Schema } = mongoose;

const parkingSpaceSchema = new Schema(
  {
    spaceNumber: { type: Number },
    isReserved: { type: Boolean, default: false },
    isBooked: { type: Boolean, default: false },
    bookedOn: { type: Date },
    bookedBy: { type: Schema.Types.ObjectId, ref: 'User' },
    arrivalTime: { type: Date },
    isAllocated: { type: Boolean },
    allocatedOn: { type: Date },
    status: { type: Boolean, default: true },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('ParkingSpace', parkingSpaceSchema);
