'use strict';

require('dotenv').config();

// node modules
const http = require('http');

// npm modules
const chalk = require('chalk');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const CronJob = require('cron').CronJob;

// app modules
const winston = require('./config/winston');
const mongoose = require('./database/mongoose');
const { urlNotFound } = require('./helpers/response');
const { requireApiKey } = require('./middlewares/apiRequest');
const cronJobController = require('./controllers/v1/cronController');

// express instance
const app = express();

// cors options
const corsOptions = {
  origin: '*',
  exposedHeaders: 'Content-Type, X-Auth-Token',
  methods: 'GET, HEAD, PUT, PATCH, POST, DELETE',
  preflightContinue: false,
};

// Database setup
mongoose.connect();

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res, next) => {
  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end('<h1>Hello World!</h1>');
});

// routes
app.use('/api/v1', requireApiKey, require('./routes/V1'));

// error handler
app.use((req, res, next) => { return next(urlNotFound()); });

app.use((err, req, res, next) => {
  const error = err;
  const status = err.status || 500;
  res.status(status).json({
    success: false,
    message: error.message ? error.message : err,
  });
});

// server setup
const port = process.env.PORT || 7000;
const server = http.createServer(app);
server.listen(port, (err) => {
  if (err) {
    console.log(chalk.red(`Error : ${err}`));
    process.exit(-1);
  }
  console.log(chalk.blue(`${process.env.APP} is running on ${port}`));
});

// At every 1 minute
var resetValues = new CronJob('* * * * *', () => { cronJobController.releaseSpace(); }, null, true);
resetValues.start();

module.exports = app;
