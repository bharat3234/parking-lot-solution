'use strict';

const passport = require('passport');
const router = require('express-promise-router')();

// controllers
const authController = require('../controllers/v1/authController');
const userController = require('../controllers/v1/userController');
const parkingController = require('../controllers/v1/parkingController');

// passport
require('../helpers/passport');

const requireAuth = passport.authenticate('jwt', { session: false });

// auth routes
router.post('/signUp', authController.signUp);
router.post('/signIn', authController.signIn);

// users routes
router.get('/users', requireAuth, userController.users);
router.get('/users/:skip/:limit', requireAuth, userController.usersWithPagiation);

// parking-space routes
router.post('/spaces', parkingController.createSpace);
router.post('/spaces/book', requireAuth, parkingController.bookSpace);
router.post('/spaces/allocate', requireAuth, parkingController.allocateSpace);
router.post('/spaces/release', requireAuth, parkingController.releaseSpace);
router.get('/spaces/booked', requireAuth, parkingController.getBookedSpaces);
router.get('/spaces/available', requireAuth, parkingController.getAvailableSpaces);
router.get('/spaces/:skip/:limit', requireAuth, parkingController.getAllSpaces);

module.exports = router;
