'use strict';

const winston = require('winston');
require('winston-daily-rotate-file');

const options = {
  info: {
    filename: `./logs/app-%DATE%.log`,
    datePattern: 'YYYY-MM-DD',
    handleExceptions: true,
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    colorize: true,
    level: 'info',
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

const logger = new winston.createLogger({
  level: 'verbose',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.colorize(),
    winston.format.align(),
    winston.format.simple(),
    winston.format.prettyPrint(),
    winston.format.printf((log) => {
      return `${log.timestamp} | ${log.level}: ${log.message}`;
    }),
  ),
  transports: [
    new winston.transports.DailyRotateFile(options.info),
    new winston.transports.Console(options.console),
  ],
  exitOnError: false,
});

logger.stream = { write(message, encoding) { logger.info(message); } };

module.exports = logger;
