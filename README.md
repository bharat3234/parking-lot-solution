
# Welcome to Parking Lot Solution 👋

This solution helps office management to automate the parking slot reservation.

## Tech Stack

**Server :** Node, Express

**Database :** MongoDB

## Requirements

You will need **Node.js** and **MongoDB** installed on your machine.

## Environment Variables

Also, you will need to add the following environment variables to your .env file

`APP`

`PORT`

`API_KEY`

`JWT_SECRET`

`MONGO_URL`

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/bharat3234/parking-lot-solution.git
```

Go to the project directory

```bash
  cd parking-lot-solution
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

Start the server with hot-reload (live-reload) enable

```bash
  npm run dev
```

## 🚀 API Documentation

- [Postman Collection](https://documenter.getpostman.com/view/12577413/UVRHh2wQ)


## Authors

- [Bharat Chilampuram](https://www.linkedin.com/in/bharat-chilampuram)

