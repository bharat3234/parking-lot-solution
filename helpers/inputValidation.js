'use strict';

const Joi = require('joi');

module.exports = {

  // auth controller
  validateSignUp: (input) => {
    const schema = Joi.object().keys({
      userName: Joi.string().required(),
      email: Joi.string().email({ minDomainAtoms: 2 }).required(),
      password: Joi.string().min(8).max(16).required(),
    });
    return Joi.validate(input, schema);
  },

  validateSignIn: (input) => {
    const schema = Joi.object().keys({
      email: Joi.string().email({ minDomainAtoms: 2 }).required(),
      password: Joi.string().required(),
    });
    return Joi.validate(input, schema);
  },

  // parking controller
  validateBookSpace: (input) => {
    const schema = Joi.object().keys({
      arrivalTime: Joi.string().required(),
      reservedBooking: Joi.boolean().required(),
    });
    return Joi.validate(input, schema);
  },

  validateAllocateSpace: (input) => {
    const schema = Joi.object().keys({
      spaceNumber: Joi.number().required(),
    });
    return Joi.validate(input, schema);
  },

  validateReleaseSpace: (input) => {
    const schema = Joi.object().keys({
      spaceNumber: Joi.number().required(),
    });
    return Joi.validate(input, schema);
  },

};
