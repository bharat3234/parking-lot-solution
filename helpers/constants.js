'use strict';

module.exports = {

  status: {
    ACTIVE: true,
    DEACTIVE: false,
  },

  totalSpaces: 120,

  reservedPercentage: 20,

  maxCapacityPercentage: 50,

  waiTimeInMinutes: 15,

};
