'use strict';

const generalLogger = require('../config/winston');

module.exports = {

  respondSuccess: (statusCode, res, message, data) => {
    generalLogger.log('info', `${message}`);
    if (!data) {
      return res.status(statusCode).json({ success: true, message: message });
    }
    return res.status(statusCode).json({ success: true, message: message, data });
  },

  respondFailure: (statusCode, res, message) => {
    generalLogger.log('info', `${message}`);
    res.status(statusCode).json({ success: false, message: message });
  },

  respondError: (status, message) => {
    generalLogger.log('error', message);
    const error = new Error(`${message}`);
    error.status = status;
    return error;
  },

  urlNotFound: (res, data) => {
    generalLogger.log('warn', 'url not found, please check the documentation');
    const error = new Error('url not found, please check the documentation');
    error.status = 404;
    return error;
  },

};
