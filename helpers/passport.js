'use strict';

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');

// models
const User = require('../models/User');

// helpers
const { respondError } = require('./response');
const constValues = require('./constants');

// Setup options for JWT Strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('authorization'),
  secretOrKey: process.env.JWT_SECRET,
  passReqToCallback: true,
};

// Create JWT strategy
const jwtLogin = new JwtStrategy(jwtOptions, (req, payload, done) => {
  User.findById(payload.sub, (err, user) => {
    if (err) {
      return done(respondError(500, 'Server is busy, Please try after sometime'), false);
    }
    if (user) {
      done(null, user);
    } else {
      done(respondError(401, 'Invalid access token'), false);
    }
  });
});

// Tell passport to use this strategy
passport.use(jwtLogin);
